<?
global $arTheme, $arRegion;
$logoClass = ($arTheme['COLORED_LOGO']['VALUE'] !== 'Y' ? '' : ' colored');
?>
<div class="mobileheader-v1">
	<div class="burger pull-left">
		<i class="svg svg-burger mask"></i>
		<i class="svg svg-close black lg"></i>
	</div>
	<div class="logo-block pull-left">
		<div class="logo<?=$logoClass?>">
			<?=CNext::ShowLogo();?>
		</div>
	</div>	
	<div class="pull-left" style="position: relative;top:17px;left:10px">
		<?$APPLICATION->IncludeComponent(
					"altasib:geobase.select.city",
					"",
					Array(
						"LOADING_AJAX" => "N",
						"RIGHT_ENABLE" => "Y",
						"SMALL_ENABLE" => "N",
						"SPAN_LEFT" => "",
						"SPAN_RIGHT" => "Выберите город"
					)
				);?>
	</div>
	<div class="right-icons pull-right hidden-xs">
		<div class="pull-right">
			<div class="wrap_icon">
				<button class="top-btn inline-search-show twosmallfont">
					<i class="svg svg-search lg" aria-hidden="true"></i>
				</button>
			</div>
		</div>
		<div class="pull-right">
			<div class="wrap_icon wrap_basket">
				<?=CNext::ShowBasketWithCompareLink('', 'lg');?>
			</div>
		</div>
		<div class="pull-right">
			<div class="wrap_icon wrap_cabinet">
				<?=CNext::showCabinetLink(true, false, 'lg');?>
			</div>
		</div>
	</div>	
</div>