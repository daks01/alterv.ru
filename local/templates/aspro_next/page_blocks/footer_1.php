<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><div class="footer_inner <?=($arTheme["SHOW_BG_BLOCK"]["VALUE"] == "Y" ? "fill" : "no_fill");?> footer-light ext_view">
	<div class="bottom_wrapper">
		<div class="wrapper_inner">
			<div class="row bottom-middle footer_address">				
				<div class="col-m-20 col-md-3 col-sm-12 col-xs-12">
					<div class="item" itemscope="" itemtype="https://schema.org/LocalBusiness">
						<meta itemprop="name" content="Промышленнная фурнитура Алтервиа">
						<meta itemprop="image" content="https://alterv.ru/images/logonew.jpg">
						<meta itemprop="priceRange" content="1-1000000000">
						<meta itemprop="address" content="г. Москва, ул. Талалихина дом 41, строение 31">
						<p class="title"><span class="square"></span>Москва</p>
						<p class="phone"><a href="tel:+74995200019" style="color: #0f0f0f"><span itemprop="telephone">+7 (499) 520-00-19</span></a></p>
						<p class="address">ул. Талалихина дом 41, строение 31</p>
						<p><span itemprop="email">info@alterv.ru</span></p>
						<p><time itemprop="openingHours" datetime="Mo-Th 09:00−18:00">Пн-Чт: 9:00-18:00</time></p>
						<p><time itemprop="openingHours" datetime="Fr 09:00−17:00">Пт: 9:00-17:00</time></p>
						<p>Сб-Вс: выходные</p>
					</div>					
				</div>
				<div class="col-m-20 col-md-3 col-sm-12 col-xs-12">
					<div class="item" itemscope="" itemtype="https://schema.org/LocalBusiness">
						<meta itemprop="name" content="Промышленнная фурнитура Алтервиа">
						<meta itemprop="image" content="https://alterv.ru/images/logonew.jpg">
						<meta itemprop="priceRange" content="1-1000000000">
						<meta itemprop="address" content="г. Новосибирск, ул. Дуси Ковальчук, дом 2/2, офис 13">
						<p class="title"><span class="square"></span>Новосибирск</p>
						<p class="phone"><a href="tel:+7383390-00-19" style="color: #0f0f0f"><span itemprop="telephone">+7 (383) 390-00-19</span></a></p>
						<p class="address">ул. Дуси Ковальчук, дом 2/2, офис 13</p>
						<p><span itemprop="email">nsk@alterv.ru</span></p>
						<p><time itemprop="openingHours" datetime="Mo-Th 09:00−18:00">Пн-Чт: 9:00-18:00</time></p>
						<p><time itemprop="openingHours" datetime="Fr 09:00−17:00">Пт: 9:00-17:00</time></p>
						<p>Сб-Вс: выходные</p>
					</div>
				</div>
				<div class="col-m-20 col-md-3 col-sm-12 col-xs-12">
					<div class="item" itemscope="" itemtype="https://schema.org/LocalBusiness">
						<meta itemprop="name" content="Промышленнная фурнитура Алтервиа">
						<meta itemprop="image" content="https://alterv.ru/images/logonew.jpg">
						<meta itemprop="priceRange" content="1-1000000000">
						<meta itemprop="address" content="г. Екатеринбург, ул. Блюхера, дом 2, офис 1">
						<p class="title"><span class="square"></span>Екатеринбург</p>
						<p class="phone"><a href="tel:+73433631063" style="color: #0f0f0f"><span itemprop="telephone">+7 (343) 363-10-63</span></a></p>
						<p class="address">ул. Блюхера, дом 2, офис 1</p>
						<p><span itemprop="email">ekb@alterv.ru</span></p>
						<p><time itemprop="openingHours" datetime="Mo-Th 09:00−18:00">Пн-Чт: 9:00-18:00</time></p>
						<p><time itemprop="openingHours" datetime="Fr 09:00−17:00">Пт: 9:00-17:00</time></p>
						<p>Сб-Вс: выходные</p>
					</div>				
				</div>
				<div class="col-m-20 col-md-3 col-sm-12 col-xs-12">
					<div class="item" itemscope="" itemtype="https://schema.org/LocalBusiness">
						<meta itemprop="name" content="Промышленнная фурнитура Алтервиа">
						<meta itemprop="image" content="https://alterv.ru/images/logonew.jpg">
						<meta itemprop="priceRange" content="1-1000000000">
						<meta itemprop="address" content="г. Ростов-на-Дону, ул. 14 линия 86, офис 223">
						<p class="title"><span class="square"></span>Ростов-на-Дону</p>
						<p class="phone"><a href="tel:+78633202019" style="color: #0f0f0f"><span itemprop="telephone">+7 (863) 320-20-19</span></a></p>
						<p class="address">ул. 14 линия 86, офис 223</p>
						<p><span itemprop="email">rnd@alterv.ru</span></p>
						<p><time itemprop="openingHours" datetime="Mo-Th 09:00−18:00">Пн-Чт: 9:00-18:00</time></p>
						<p><time itemprop="openingHours" datetime="Fr 09:00−17:00">Пт: 9:00-17:00</time></p>
						<p>Сб-Вс: выходные</p>
					</div>				
				</div>
				<div class="col-m-20 col-md-3 col-sm-12 col-xs-12">
					<div class="item" itemscope="" itemtype="https://schema.org/LocalBusiness">
						<meta itemprop="name" content="Промышленнная фурнитура Алтервиа">
						<meta itemprop="image" content="https://alterv.ru/images/logonew.jpg">
						<meta itemprop="priceRange" content="1-1000000000">
						<meta itemprop="address" content="г. Санкт-Петербург, пр. Стачек 45 кор 2 лит А, офис 105">
						<p class="title"><span class="square"></span>Санкт-Петербург</p>
						<p class="phone"><a href="tel:+78126136312" style="color: #0f0f0f"><span itemprop="telephone">+7 (812) 613-63-12</span></a></p>
						<p class="address">пр. Стачек 45 кор 2 лит А, офис 105</p>
						<p><span itemprop="email">spb@alterv.ru</span></p>
						<p><time itemprop="openingHours" datetime="Mo-Th 09:00−18:00">Пн-Чт: 9:00-18:00</time></p>
						<p><time itemprop="openingHours" datetime="Fr 09:00−17:00">Пт: 9:00-17:00</time></p>
						<p>Сб-Вс: выходные</p>
					</div>				
				</div>
				<div class="col-m-20 col-md-3 col-sm-12 col-xs-12" style="display: none">
					<div class="item" itemscope="" itemtype="https://schema.org/LocalBusiness">
						<meta itemprop="name" content="Промышленнная фурнитура Алтервиа">
						<meta itemprop="image" content="https://alterv.ru/images/logonew.jpg">
						<meta itemprop="priceRange" content="1-1000000000">
						<p class="title"><span class="square"></span>Дилер</p>
						<p class="phone"><a href="tel:+74852599320" style="color: #0f0f0f"><span itemprop="telephone">+7 (4852) 59-93-20</span></a></p>
						<p class="address"><span itemprop="address">"Центр Режущего Инструмента", г. Ярославль, Большая Фёдоровская, 118а</span></p>
						<p><span itemprop="email">476872@list.ru</span></p>
						<p><time itemprop="openingHours" datetime="Mo-Th 09:00−18:00">Пн-Чт: 9:00-18:00</time></p>
						<p><time itemprop="openingHours" datetime="Fr 09:00−17:00">Пт: 9:00-17:00</time></p>
						<p>Сб-Вс: выходные</p>
					</div>					
				</div>
			</div>			
		</div>
	</div>		
	<?$APPLICATION->AddViewContent('mycontent', 'данные');?>
	<div class="maxwidth-theme bottom-under">
				<div class="row">
					<div class="col-md-12 outer-wrapper">
						<div class="inner-wrapper row">
							<div class="copy-block">
								<div class="copy">
									<?$APPLICATION->IncludeFile(SITE_DIR."include/footer/copy/copyright.php", Array(), Array(
											"MODE" => "php",
											"NAME" => "Copyright",
											"TEMPLATE" => "include_area.php",
										)
									);?>
								</div>
								<div class="print-block"><a href="/karta-sayta/">карта сайта</a><?=CNext::ShowPrintLink();?></div>
								<div id="bx-composite-banner"></div>
							</div>
							<div class="pull-right pay_system_icons">
								<span class="">
									<?$APPLICATION->IncludeFile(SITE_DIR."include/footer/copy/pay_system_icons.php", Array(), Array("MODE" => "html", "NAME" => GetMessage("PHONE"), "TEMPLATE" => "include_area.php",));?>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
</div>