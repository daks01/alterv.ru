<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
global $arTheme, $arRegion;
$arRegions = CNextRegionality::getRegions();
if($arRegion)
	$bPhone = ($arRegion['PHONES'] ? true : false);
else
	$bPhone = ((int)$arTheme['HEADER_PHONES'] ? true : false);
$logoClass = ($arTheme['COLORED_LOGO']['VALUE'] !== 'Y' ? '' : ' colored');
?>

<div class="header2" id="">
	<div class="wrapper_inner">
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<div class="logo<?=$logoClass?>">
					<?if($APPLICATION->sDirPath=='/'):?>
						<img src="/images/logonew.jpg" style="width: 208px;height: 74px" alt="Промышленнная фурнитура Алтервиа">
					<?else:?>
						<a href="/"><img src="/images/logonew.jpg" style="width: 208px;height: 74px" alt="Промышленнная фурнитура Алтервиа"></a>					
					<?endif?>					
				</div>
				<div class="top-description2" style="height: auto;margin:-5px 0 0 55px;font-size: 10.8px;color: #264796;text-shadow: 0px 0px 0px #264796">
					<?if($APPLICATION->sDirPath=='/'):?>
						<?$APPLICATION->IncludeFile(SITE_DIR."include/top_page/slogan.php", array(), array(
							"MODE" => "html",
							"NAME" => "Text in title",
							"TEMPLATE" => "include_area.php",
						));?>
					<?else:?>
						<a href="/">
							<?$APPLICATION->IncludeFile(SITE_DIR."include/top_page/slogan.php", array(), array(
								"MODE" => "html",
								"NAME" => "Text in title",
								"TEMPLATE" => "include_area.php",
							));?>
						</a>
					<?endif?>					
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<?$APPLICATION->IncludeComponent(
					"altasib:geobase.select.city",
					"",
					Array(
						"LOADING_AJAX" => "N",
						"RIGHT_ENABLE" => "N",
						"SMALL_ENABLE" => "Y",
						"SPAN_LEFT" => "Мой город:",
						"SPAN_RIGHT" => "Выберите город"
					)
				);?>
			</div>
			<div class="col-lg-4 col-md-4 contact_block">
				<div class="address"><?=$_COOKIE['address']?></div>
				<div class="phone"><?=$_COOKIE['phone']?></div>
				<?if($arTheme['SHOW_CALLBACK']['VALUE'] == 'Y'):?>
					<div class="callback" data-event="jqm" data-param-form_id="CALLBACK" data-name="callback"><img src="/images/site/group-6.png" class="icon"><?=GetMessage("CALLBACK")?></div>
				<?endif;?>
			</div>
		</div>
	</div>
</div>

<div class="menu-new"><div class="wrapper_inner ">
	<div class="row">
		<div class="col-md-7 menu-row">
			<div class="nav-main-collapse collapse in">
				<div class="menu-only">
					<nav class="mega-menu sliced">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
							array(
								"COMPONENT_TEMPLATE" => ".default",
								"PATH" => SITE_DIR."include/menu/menu.top.php",
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "",
								"AREA_FILE_RECURSIVE" => "Y",
								"EDIT_TEMPLATE" => "include_area.php"
								),
							false, array("HIDE_ICONS" => "Y")
						);?>
					</nav>
				</div>
			</div>
		</div>
		<div class="col-md-5 search-row">
			<form action="/catalog/">				
				<input type="text" name="q" value="" placeholder="Поиск по сайту">
				<button class="" type="submit" name="s" value="Поиск"><img src="/images/site/srch.png"></button>
			</form>
		</div>
	</div>
</div>
</div>

<div class="top-block top-block-v1" style="display: none">
	<div class="maxwidth-theme">		
		<div class="wrapp_block">
			<div class="row">
				<?if($arRegions):?>
					<div class="top-block-item col-md-2">
						<div class="top-description">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/top_page/regionality.list.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "include_area.php"
								),
								false
							);?>
						</div>
					</div>
				<?else:?>
					<div class="top-block-item col-md-4">
						<div class="phone-block">
							<?if($bPhone):?>
								<div class="inline-block">
									<?CNext::ShowHeaderPhones();?>
								</div>
							<?endif?>
							<?if($arTheme['SHOW_CALLBACK']['VALUE'] == 'Y'):?>
								<div class="inline-block">
									<span class="callback-block animate-load twosmallfont colored" data-event="jqm" data-param-form_id="CALLBACK" data-name="callback"><?=GetMessage("CALLBACK")?></span>
								</div>
							<?endif;?>
						</div>
					</div>
				<?endif;?>
				<div class="top-block-item pull-left visible-lg">
					<?CNext::showAddress('address twosmallfont inline-block');?>
				</div>
				<div class="top-block-item pull-right show-fixed top-ctrl">
					<button class="top-btn inline-search-show twosmallfont">
						<i class="svg svg-search" aria-hidden="true"></i>
						<span class="dark-color"><?=GetMessage('SEARCH_TITLE')?></span>
					</button>
				</div>
				<div class="top-block-item pull-right show-fixed top-ctrl">
					<div class="basket_wrap twosmallfont">
						<?CNext::ShowBasketWithCompareLink('', '');?>
					</div>
				</div>
				<div class="top-block-item pull-right show-fixed top-ctrl">
					<div class="personal_wrap">
						<div class="personal top login twosmallfont">
							<?=CNext::ShowCabinetLink(true, true);?>
						</div>
					</div>
				</div>
				<?if($arRegions):?>
					<div class="top-block-item pull-right">
						<div class="phone-block">
							<?if($bPhone):?>
								<div class="inline-block">
									<?CNext::ShowHeaderPhones();?>
								</div>
							<?endif?>
							<?if($arTheme['SHOW_CALLBACK']['VALUE'] == 'Y'):?>
								<div class="inline-block">
									<span class="callback-block animate-load twosmallfont colored" data-event="jqm" data-param-form_id="CALLBACK" data-name="callback"><?=GetMessage("CALLBACK")?></span>
								</div>
							<?endif;?>
						</div>
					</div>
				<?endif;?>
			</div>
		</div>
	</div>
</div>
<div class="header-wrapper topmenu-LIGHT" style="display: none">
	<div class="wrapper_inner">
		<div class="logo_and_menu-row">
			<div class="logo-row row">
				<div class="logo-block col-md-4 col-sm-3">
					<div class="logo<?=$logoClass?>" style="max-width: 100%">
						<a href="/"><img src="/images/logonew.jpg"></a>
						<?//=CNext::ShowLogo();?>						
					</div>
					<div class="top-description2" style="height: auto;margin:0 0 20px 77px">
							<?$APPLICATION->IncludeFile(SITE_DIR."include/top_page/slogan.php", array(), array(
									"MODE" => "html",
									"NAME" => "Text in title",
									"TEMPLATE" => "include_area.php",
								)
							);?>
					</div>
				</div>				
				<div class="col-md-8 menu-row">
					<div class="nav-main-collapse collapse in">
						<div class="menu-only">
							<nav class="mega-menu sliced">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
									array(
										"COMPONENT_TEMPLATE" => ".default",
										"PATH" => SITE_DIR."include/menu/menu.top.php",
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "",
										"AREA_FILE_RECURSIVE" => "Y",
										"EDIT_TEMPLATE" => "include_area.php"
									),
									false, array("HIDE_ICONS" => "Y")
								);?>
							</nav>
						</div>
					</div>
				</div>
			</div><?// class=logo-row?>
		</div>
	</div>
	<div class="line-row visible-xs"></div>
</div>