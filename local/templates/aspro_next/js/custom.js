/*
You can use this file with your scripts.
It will not be overwritten when you upgrade solution.
*/
$(document).ready(function(){
	$('*[data-region_id]').click(function(e){
		document.location.href = $(this).attr('data-href');
		e.preventDefault;
		return false;
	});
	
	$(".tab-content .detail_text table.catalog_decs").wrap("<div class='table-responsive'></div>");
});