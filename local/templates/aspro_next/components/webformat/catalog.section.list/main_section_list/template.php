<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>


<?if($arResult["SECTIONS"]):?>
<div class="catalog_section_list row items sections_wrapper">	
	<?foreach( $arResult["SECTIONS"] as $arItems ):
		$this->AddEditAction($arItems['ID'], $arItems['EDIT_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItems['ID'], $arItems['DELETE_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	
		<div class="main_title p<?=$arItems['ID']?>">
			<a href="<?=$arItems["SECTION_PAGE_URL"]?>"><?=$arItems["NAME"]?></a> 
		</div>
		<?if($arItems["SECTIONS"]):?>
			<div class="list items">
				<div class="row margin0">
				<? $max_show_elem = 10; ?>
				
				<?if($arItems["ID"]==80):?>
					<div class="col-m-37 col-md-3 col-sm-6 col-xs-12 cont_more">
						<a href="/catalog/promyshlennaya_furnitura/">
						<img src="/images/site/bolts.jpg">
						<div class="info">
							<div class="col-sm-7">
								<p>Полный ассортимент товаров вы можете посмотреть</p>
							</div>
							<div class="col-sm-5">
								<a class="incat" href="/catalog/promyshlennaya_furnitura/">В каталоге</a>
							</div>
						</div>
						</a>
					</div>
				<?endif?>
				
				<?foreach( $arItems["SECTIONS"] as $arItem ):?>
					<?
						$arSelect = Array("PROPERTY_219");
						$arFilter = Array("SECTION_ID"=>$arItem["ID"], "ACTIVE"=>"Y");
						$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>500), $arSelect);						
					?>
					<?if($max_show_elem--<=0){break;}?>
					<div class="col-m-20 col-md-3 col-sm-4 col-xs-12 parent_all_link">
						<?
							while($ob = $res->GetNextElement()){
								$arFields = $ob->GetFields();
								if(isset($arFields['PROPERTY_219_VALUE'])){
									$proc[$arItem["ID"]][] = $arFields["PROPERTY_219_VALUE"];
								}							
							}
							
							$skidka = CIBlockSection::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => 17, "ID" =>$arItem['ID']), false, $arSelect = array("NAME","UF_SKIDKA"))->GetNext()["UF_SKIDKA"];							
						?>						
						<a href="<?=$arItem["SECTION_PAGE_URL"]?>" class="thumb all_link"></a>
						<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<?if($skidka!=''):?>
								<span class="skidka">-<?=$skidka?>%</span>
							<?endif?>							
							<div class="img shine">								
								<?if($arItem["PICTURE"]["SRC"]):?>
									<?$img = CFile::ResizeImageGet($arItem["PICTURE"]["ID"], array( "width" => 120, "height" => 120 ), BX_RESIZE_IMAGE_EXACT, true );?>
									<!-- <a href="<?=$arItem["SECTION_PAGE_URL"]?>" class="thumb"> -->
										<img src="<?=$img["src"]?>" alt="<?=($$arItem["PICTURE"]["ALT"] ? $arItem["PICTURE"]["ALT"] : $arItem["NAME"])?>" title="<?=($arItem["PICTURE"]["TITLE"] ? $arItem["PICTURE"]["TITLE"] : $arItem["NAME"])?>" />
									<!-- </a> -->
								<?elseif($arItem["~PICTURE"]):?>
									<?$img = CFile::ResizeImageGet($arItem["~PICTURE"], array( "width" => 120, "height" => 120 ), BX_RESIZE_IMAGE_EXACT, true );?>
									<a href="<?=$arItem["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=$img["src"]?>" alt="<?=($$arItem["PICTURE"]["ALT"] ? $arItem["PICTURE"]["ALT"] : $arItem["NAME"])?>" title="<?=($arItem["PICTURE"]["TITLE"] ? $$arItem["PICTURE"]["TITLE"] : $arItem["NAME"])?>" /></a>
								<?else:?>
									<a href="<?=$arItem["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" height="90" /></a>
								<?endif;?>
							</div>
							<div class="name title_name1" >
								<a href="<?=$arItem['SECTION_PAGE_URL'];?>" class="dark_link"><?=$arItem['NAME'];?></a>
							</div>
						</div>
					</div>
				<?endforeach;?>					
				</div>
<!-- 				<div class="row margin0">

				</div> -->

			</div>
		<?endif;?>
	<?endforeach;?>
	<div class="main_title tt">
		<a>Топ товаров</a> 
	</div>
	<div class="list items">
		<div class="row margin0">
			<div class="owl-carousel top_on_main_slider">
				<?
					$arSelect = Array("ID","PROPERTY_135","NAME","PROPERTY_121","PROPERTY_122","DETAIL_PAGE_URL","DETAIL_PICTURE","PROPERTY_219");
					$arFilter = Array("IBLOCK_ID"=>17, "ACTIVE"=>"Y","PROPERTY_123_ENUM_ID"=>82);
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
				?>
				<?while($ob = $res->GetNextElement()):?>
					<?
						$arFields = $ob->GetFields();
						$img = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array( "width" => 150, "height" => 150 ), true );
						
						if($arFields["PROPERTY_219_VALUE"]!=''){
							$proc = round(str_replace(',','.',$arFields["PROPERTY_121_VALUE"])-(str_replace(',','.',$arFields["PROPERTY_121_VALUE"])*$arFields["PROPERTY_219_VALUE"]/100),2);
						}						
					?>
					<div class="top_on_main">
						<?if($arFields["PROPERTY_219_VALUE"]!=''):?>
						<span class="skidka">-<?=$arFields["PROPERTY_219_VALUE"]?>%</span>
						<?endif?>
						<a href="<?=$arFields["DETAIL_PAGE_URL"]?>"><img src="<?=$img["src"]?>"></a>
						<p class="artikul"><a href="<?=$arFields["DETAIL_PAGE_URL"]?>"><?=$arFields["PROPERTY_135_VALUE"]?></a></p>
						<p class="name"><?=explode($arFields["PROPERTY_135_VALUE"].' ',$arFields["NAME"])[1]?></p>
						<?if($arFields["PROPERTY_219_VALUE"]!=''):?>
							<p class="price">от <?=$proc?> Р</p>							
							<p style="text-decoration: line-through">от <?=$arFields["PROPERTY_121_VALUE"]?> Р<p>
						<?else:?>
							<p class="price">от <?=$arFields["PROPERTY_121_VALUE"]?><p>
						<?endif?>
						
						<!--a class="more" onclick="oneClickBuy(<?=$arFields["ID"]?>, '17', this)">купить</a-->
						<a class="more one_click" data-href="<?=$_SERVER['HTTP_HOST'].$arFields["DETAIL_PAGE_URL"]?>" data-toggle="modal" data-target="#by1click">купить</a>
					</div>
				<?endwhile?>
			</div>
		</div>
	</div>
</div>
<?endif;?>