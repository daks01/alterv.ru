<?$arResult = CNext::getChilds2($arResult);
global $arRegion;
if($arResult)
{
	foreach($arResult as $key=>$arItem)
	{
		if(isset($arItem['PARAMS']) && $arRegion)
		{
			if(isset($arItem['PARAMS']['LINK_REGION']))
			{
				if($arItem['PARAMS']['LINK_REGION'])
				{
					if(!in_array($arRegion['ID'], $arItem['PARAMS']['LINK_REGION']))
						unset($arResult[$key]);
				}
				else
					unset($arResult[$key]);
			}
		}
		if($arItem["CHILD"])
			$arResult[$key]["CHILD"]=CNext::unique_multidim_array($arItem["CHILD"], "TEXT");
	}
}?>