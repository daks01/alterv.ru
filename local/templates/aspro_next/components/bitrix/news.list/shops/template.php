<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if($arParams['IBLOCK_ID'] == 23){
	$cData = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].$this->GetFolder().'/coords.txt'));
	foreach($arResult['ITEMS'] as $k => $arItem){
		if(strlen($arResult['ITEMS'][$k]['PROPERTIES']['MAP']['VALUE']) == 0 && strlen($arItem['PROPERTIES']['ADDRESS']['VALUE']) > 0){
			$params = array(
				'geocode' => $arItem['PROPERTIES']['ADDRESS']['VALUE'], // адрес
				'format'  => 'json',                          // формат ответа
				'results' => 1,                               // количество выводимых результатов
				'key'     => '...',                           // ваш api key
			);
			$response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));

			if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0)
			{
				$ccc = explode(' ', $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
			}
			CIBlockElement::SetPropertyValuesEx($arItem['ID'], false, array('MAP' => $ccc[1].', '.$ccc[0]));
			$up++;
		}
	}
	if($up > 0){
		?>
		
		<?
	}
}?>
<?//CNext::drawShopsList($arResult['ITEMS'], $arParams, "N", $_COOKIE['current_region']);?>

<?
$arShops = $arResult['ITEMS'];
		global $APPLICATION;
		$mapLAT = $mapLON = 0;
		$arPlacemarks = array();

		if(is_array($arShops)){
			foreach($arShops as $i => $arShop){
				if(isset($arShop['IBLOCK_ID'])){
					$arShop['TITLE'] = (in_array('NAME', $arParams['FIELD_CODE']) ? strip_tags($arShop['~NAME']) : '');

					$imageID = ((in_array('PREVIEW_PICTURE', $arParams['FIELD_CODE']) && $arShop["PREVIEW_PICTURE"]['ID']) ? $arShop["PREVIEW_PICTURE"]['ID'] : ((in_array('DETAIL_PICTURE', $arParams['FIELD_CODE']) && $arShop["DETAIL_PICTURE"]['ID']) ? $arShop["DETAIL_PICTURE"]['ID'] : false));
					$arShop['IMAGE'] = ($imageID ? CFile::ResizeImageGet($imageID, array('width' => 100, 'height' => 69), BX_RESIZE_IMAGE_EXACT) : '');
					$arShop['ADDRESS'] = $arShop['DISPLAY_PROPERTIES']['ADDRESS']['VALUE'];
					$arShop['ADDRESS'] = $arShop['TITLE'].((strlen($arShop['TITLE']) && strlen($arShop['ADDRESS'])) ? ', ' : '').$arShop['ADDRESS'];
					$arShop['PHONE'] =  $arShop['DISPLAY_PROPERTIES']['PHONE']['VALUE'];
					$arShop['EMAIL'] = $arShop['DISPLAY_PROPERTIES']['EMAIL']['VALUE'];

					if(strToLower($arShop['DISPLAY_PROPERTIES']['SCHEDULE']['VALUE']['TYPE']) == 'html'){
						$arShop['SCHEDULE'] = htmlspecialchars_decode($arShop['DISPLAY_PROPERTIES']['SCHEDULE']['~VALUE']['TEXT']);
					}
					else{
						$arShop['SCHEDULE'] = nl2br($arShop['DISPLAY_PROPERTIES']['SCHEDULE']['~VALUE']['TEXT']);
					}
					$arShop['URL'] = $arShop['DETAIL_PAGE_URL'];
					$arShop['METRO_PLACEMARK_HTML'] = '';
					if($arShop['METRO'] = $arShop['DISPLAY_PROPERTIES']['METRO']['VALUE']){
						if(!is_array($arShop['METRO'])){
							$arShop['METRO'] = array($arShop['METRO']);
						}
						foreach($arShop['METRO'] as $metro){
							$arShop['METRO_PLACEMARK_HTML'] .= '<div class="metro"><i></i>'.$metro.'</div>';
						}
					}
					$arShop['DESCRIPTION'] = $arShop['DETAIL_TEXT'];
					$arShop['GPS_S'] = false;
					$arShop['GPS_N'] = false;
					if($arStoreMap = explode(',', $arShop['DISPLAY_PROPERTIES']['MAP']['VALUE'])){
						$arShop['GPS_S'] = $arStoreMap[0];
						$arShop['GPS_N'] = $arStoreMap[1];
					}
					if($arShop['GPS_S'] && $arShop['GPS_N']){
						$mapLAT += $arShop['GPS_S'];
						$mapLON += $arShop['GPS_N'];
						$str_phones = '';
						if($arShop['PHONE'])
						{
							foreach($arShop['PHONE'] as $phone)
							{
								$str_phones .= '<div class="phone"><a rel="nofollow" href="tel:'.str_replace(array(' ', ',', '-', '(', ')'), '', $phone).'">'.$phone.'</a></div>';
							}
						}
						$arPlacemarks[] = array(
							"ID" => $arShop["ID"],
							"LAT" => $arShop['GPS_S'],
							"LON" => $arShop['GPS_N'],
							"TEXT" => $arShop["TITLE"],
							"HTML" => '<div class="title">'.(strlen($arShop["URL"]) ? '<a href="'.$arShop["URL"].'">' : '').$arShop["ADDRESS"].(strlen($arShop["URL"]) ? '</a>' : '').'</div><div class="info-content">'.($arShop['METRO'] ? $arShop['METRO_PLACEMARK_HTML'] : '').(strlen($arShop['SCHEDULE']) ? '<div class="schedule">'.$arShop['SCHEDULE'].'</div>' : '').$str_phones.(strlen($arShop['EMAIL']) ? '<div class="email"><a rel="nofollow" href="mailto:'.$arShop['EMAIL'].'">'.$arShop['EMAIL'].'</a></div>' : '').'</div>'.(strlen($arShop['URL']) ? '<a rel="nofollow" class="button" href="'.$arShop["URL"].'"><span>'.GetMessage('DETAIL').'</span></a>' : '')
						);
					}
				}
				else{
					$str_phones = '';
					if($arShop['PHONE'])
					{
						$arShop['PHONE'] = explode(",", $arShop['PHONE']);
						foreach($arShop['PHONE'] as $phone)
						{
							$str_phones .= '<div class="phone"><a rel="nofollow" href="tel:'.str_replace(array(' ', ',', '-', '(', ')'), '', $phone).'">'.$phone.'</a></div>';
						}
					}
					if($arShop['GPS_S'] && $arShop['GPS_N']){
						$mapLAT += $arShop['GPS_N'];
						$mapLON += $arShop['GPS_S'];
						$arPlacemarks[] = array(
							"ID" => $arShop["ID"],
							"LON" => $arShop['GPS_S'],
							"LAT" => $arShop['GPS_N'],
							"TEXT" => strip_tags($arShop["TITLE"]),
							"HTML" => '<div class="title">'.(strlen($arShop["URL"]) ? '<a href="'.$arShop["URL"].'">' : '').$arShop["ADDRESS"].(strlen($arShop["URL"]) ? '</a>' : '').'</div><div class="info-content">'.($arShop['METRO'] ? $arShop['METRO_PLACEMARK_HTML'] : '').(strlen($arShop['SCHEDULE']) ? '<div class="schedule">'.$arShop['SCHEDULE'].'</div>' : '').$str_phones.(strlen($arShop['EMAIL']) ? '<div class="email"><a rel="nofollow" href="mailto:'.$arShop['EMAIL'].'">'.$arShop['EMAIL'].'</a></div>' : '').'</div>'.(strlen($arShop['URL']) ? '<a rel="nofollow" class="button" href="'.$arShop["URL"].'"><span>'.GetMessage('DETAIL').'</span></a>' : '')
						);
					}
				}
				$arShops[$i] = $arShop;
			}
			?>
			<?if($arShops):?>
			
				<?if(abs($mapLAT) > 0 && abs($mapLON) > 0 && $showMap=="Y"):?>
					<?
					$mapLAT = floatval($mapLAT / count($arShops));
					$mapLON = floatval($mapLON / count($arShops));
					?>
					<div class="contacts_map">
						<?if($arParams["MAP_TYPE"] != "0"):?>
							<?$APPLICATION->IncludeComponent(
								"bitrix:map.google.view",
								"map",
								array(
									"INIT_MAP_TYPE" => "ROADMAP",
									"MAP_DATA" => serialize(array("google_lat" => $mapLAT, "google_lon" => $mapLON, "google_scale" => 15, "PLACEMARKS" => $arPlacemarks)),
									"MAP_WIDTH" => "100%",
									"MAP_HEIGHT" => "400",
									"CONTROLS" => array(
									),
									"OPTIONS" => array(
										0 => "ENABLE_DBLCLICK_ZOOM",
										1 => "ENABLE_DRAGGING",
									),
									"MAP_ID" => "",
									"ZOOM_BLOCK" => array(
										"POSITION" => "right center",
									),
									"API_KEY" => $arParams["GOOGLE_API_KEY"],
									"COMPOSITE_FRAME_MODE" => "A",
									"COMPOSITE_FRAME_TYPE" => "AUTO"
								),
								false, array("HIDE_ICONS" =>"Y")
							);?>
						<?else:?>
							<?$APPLICATION->IncludeComponent(
								"bitrix:map.yandex.view",
								"",
								array(
									"INIT_MAP_TYPE" => "ROADMAP",
									"MAP_DATA" => serialize(array("yandex_lat" => $mapLAT, "yandex_lon" => $mapLON, "yandex_scale" => 4, "PLACEMARKS" => $arPlacemarks)),
									"MAP_WIDTH" => "100%",
									"MAP_HEIGHT" => "400",
									"CONTROLS" => array(
										0 => "ZOOM",
										1 => "SMALLZOOM",
										3 => "TYPECONTROL",
										4 => "SCALELINE",
									),
									"OPTIONS" => array(
										0 => "ENABLE_DBLCLICK_ZOOM",
										1 => "ENABLE_DRAGGING",
									),
									"MAP_ID" => "",
									"ZOOM_BLOCK" => array(
										"POSITION" => "right center",
									),
									"COMPONENT_TEMPLATE" => "map",
									"API_KEY" => $arParams["GOOGLE_API_KEY"],
									"COMPOSITE_FRAME_MODE" => "A",
									"COMPOSITE_FRAME_TYPE" => "AUTO"
								),
								false, array("HIDE_ICONS" =>"Y")
							);?>
						<?endif;?>
					</div>
				<?endif;?>
				<div class="wrapper_inner">
					<div class="shops list">
						<div class="items">							
							<br>																					
							<h1>Контакты</h1>

							
							<?foreach($arShops as $arShop):
								if($arShop["IBLOCK_ID"] == 23){
									unset($arShop['URL']);
								}
							?>
								<?if($arShop['NAME'] == $_COOKIE['regional_city']):?>													
									<div class="item <?=(strlen($arShop["IMAGE"]["src"]) ? '' : 'wi')?> contacts" data-ID="<?=$arShop['ID']?>">
										<div class="image">
											<?if(strlen($arShop["IMAGE"]["src"])):?>
												<?if(strlen($arShop['URL'])):?>
													<a href="<?=$arShop['URL']?>"><img src="<?=$arShop["IMAGE"]["src"]?>" alt="<?=$arShop["ADDRESS"]?>" title="<?=$arShop["ADDRESS"]?>" /></a>
												<?else:?>
													<img src="<?=$arShop["IMAGE"]["src"]?>" alt="<?=$arShop["ADDRESS"]?>" title="<?=$arShop["ADDRESS"]?>" />
												<?endif;?>
											<?endif;?>
										</div>
										<div class="rubber">
											<div class="title_metro">
												<?if(strlen($arShop["ADDRESS"])):?>
													<?if(strlen($arShop['URL'])):?>
														<div class="title"><a href="<?=$arShop['URL']?>"><?=$arShop["ADDRESS"]?></a></div>
													<?else:?>
														<div class="title"><?=$arShop["ADDRESS"]?></div>
													<?endif;?>
												<?endif;?>
												<?if($arShop["METRO"]):?>
													<?foreach($arShop['METRO'] as $metro):?>
														<div class="metro"><i></i><?=$metro?></div>
													<?endforeach;?>
												<?endif;?>
											</div>
											<div class="schedule_phone_email">
												<div class="schedule"><?=$arShop["SCHEDULE"]?></div>
												<div class="phone_email">
													<?if($arShop["PHONE"]):?>
														<?foreach($arShop["PHONE"] as $phone):?>
															<div class="phone"><a rel="nofollow" href="tel:<?=str_replace(array(' ', ',', '-', '(', ')'), '', $phone);?>"><?=$phone;?></a></div>
														<?endforeach;?>
													<?endif;?>
													<?if(strlen($arShop["EMAIL"])):?>
														<div class="email"><a rel="nofollow" href="mailto:<?=$arShop["EMAIL"]?>"><?=$arShop["EMAIL"]?></a></div>
													<?endif;?>
												</div>
											</div>
										</div>
										<div class="row personal">
											<h3>Персональные контакты</h3>
											<?foreach($arShop["PROPERTIES"]["SOTRUDNIKI"]["VALUE"] as $id):?>												
												<?
													$res = CIBlockElement::GetByID($id);
													$ob = $res->GetNextElement();
													$ar_res = $ob->GetFields();
													$param_ar_res = $ob->GetProperties();
												?>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 personal_item">
													<div class="col-sm-5 col-xs-12">
														<img src="<?=CFile::GetPath($ar_res['PREVIEW_PICTURE']);?>" style="max-width: 100%">
													</div>
													<div class="col-sm-7 col-xs-12">
														<p><b><?=$ar_res['NAME'];?></b></p>
														<p><?=$param_ar_res['POST']["VALUE"];?></p>														
														<p class="phone_line"><img src="/images/phone.jpg"><a href="tel:<?=$param_ar_res['PHONE']["VALUE"];?>"><?=$param_ar_res['PHONE']["VALUE"];?></a></p>
														<p class="mail_line"><img src="/images/mail.jpg"><a href="mailto:<?=$param_ar_res['EMAIL']["VALUE"];?>"><?=$param_ar_res['EMAIL']["VALUE"];?></a></p>																										
													</div>																										
												</div>																								
											<?endforeach?>
										</div>
										<div class="callback">
											<h3>Форма обратной связи</h3>
											<form action="" method="post">
												<div class="row">
													<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
														<div class="form-group">
															<input type="text" name="fio" placeholder="ФИО*" required="">
														</div>
														<div class="form-group">
															<input type="text" name="company" placeholder="Название компании">
														</div>
														<div class="form-group">
															<input type="text" name="phone" placeholder="Телефон*" required="">
															<input type="text" name="workphone" placeholder="Рабочий телефон" class="workphone">
														</div>
														<div class="form-group">
															<input type="text" name="email" placeholder="E-mail">
														</div>														
													</div>
													<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
														<div class="form-group">
															<textarea placeholder="Сообщение" name="message" rows="5"></textarea>
														</div>
														<div class="col-sm-6 col-xs-12">
															<p>Поля со * обязательны для заполнения.</p>
														</div>
														<div class="col-sm-6 col-xs-12">
															<input type="submit" name="submit_contacts" class="btn btn-primary" value="Отправить сообщение" style="float: right; background: #1976d2; border: none">
														</div>														
													</div>
												</div>
											</form>
											<?
												if(isset($_POST) && $_POST['workphone']=='')
												{
													if(preg_match("/^[0-9а-я ]+$/iu", $_POST['fio']))
													{		
														$to = $arShop["EMAIL"];
														//$to = 'vz@up-promo.pro';
												
													    // тема письма
													    $subject = 'Обратная связь';
													        
													    // текст письма
													    $message = '
													    <p>ФИО: '.$_POST['fio'].'</p>
													    <p>Компания: '.$_POST['company'].'</p>
													    <p>Номер телефона: '.$_POST['phone'].'</p>
													    <p>E-mail: '.$_POST['email'].'</p>
													    <p>Сообщение: '.$_POST['message'].'</p>';;
													
												        // Для отправки HTML-письма должен быть установлен заголовок Content-type
												        $headers = 'MIME-Version: 1.0' . "\r\n";
												        $headers .= "Content-type: text/html; charset=utf-8 \r\n";
													    $headers .= "From: info <info@alterv.ru>\r\n";
													
													    // Отправляем
												        mail($to, $subject, $message, $headers);
											        }
												}	
											?>
										</div>
										<div class="map"><?=$arShop["DISPLAY_PROPERTIES"]["MAP"]["DISPLAY_VALUE"]?></div>
									</div>								
								<?endif;?>
							<?endforeach;?>
							<?foreach($arShops as $arShop):
								if($arShop["IBLOCK_ID"] == 23){
									unset($arShop['URL']);
								}
							?>
								<?if($arShop['NAME'] != $cN):?>																
									<div class="item <?=(strlen($arShop["IMAGE"]["src"]) ? '' : 'wi')?>" data-ID="<?=$arShop['ID']?>">
										<div class="image">
											<?if(strlen($arShop["IMAGE"]["src"])):?>
												<?if(strlen($arShop['URL'])):?>
													<a href="<?=$arShop['URL']?>"><img src="<?=$arShop["IMAGE"]["src"]?>" alt="<?=$arShop["ADDRESS"]?>" title="<?=$arShop["ADDRESS"]?>" /></a>
												<?else:?>
													<img src="<?=$arShop["IMAGE"]["src"]?>" alt="<?=$arShop["ADDRESS"]?>" title="<?=$arShop["ADDRESS"]?>" />
												<?endif;?>
											<?endif;?>
										</div>
										<div class="rubber">
											<div class="title_metro">
												<?if(strlen($arShop["ADDRESS"])):?>
													<?if(strlen($arShop['URL'])):?>
														<div class="title"><a href="<?=$arShop['URL']?>"><?=$arShop["ADDRESS"]?></a></div>
													<?else:?>
														<div class="title"><?=$arShop["ADDRESS"]?></div>
													<?endif;?>
												<?endif;?>
												<?if($arShop["METRO"]):?>
													<?foreach($arShop['METRO'] as $metro):?>
														<div class="metro"><i></i><?=$metro?></div>
													<?endforeach;?>
												<?endif;?>
											</div>
											<div class="schedule_phone_email">
												<div class="schedule"><?=$arShop["SCHEDULE"]?></div>
												<div class="phone_email">
													<?if($arShop["PHONE"]):?>
														<?foreach($arShop["PHONE"] as $phone):?>
															<div class="phone"><a rel="nofollow" href="tel:<?=str_replace(array(' ', ',', '-', '(', ')'), '', $phone);?>"><?=$phone;?></a></div>
														<?endforeach;?>
													<?endif;?>
													<?if(strlen($arShop["EMAIL"])):?>
														<div class="email"><a rel="nofollow" href="mailto:<?=$arShop["EMAIL"]?>"><?=$arShop["EMAIL"]?></a></div>
													<?endif;?>
												</div>
											</div>
										</div>										
									</div>								
								<?endif;?>
							<?endforeach;?>
						</div>
												
					</div>					
				</div>
				<div class="clearboth"></div>
			<?endif;?>
			<?
		}
?>


<?if($arParams["DISPLAY_BOTTOM_PAGER"]){?>
	<hr/>
	<?=$arResult["NAV_STRING"]?>
<?}?>