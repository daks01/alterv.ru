<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams['IBLOCK_ID'] == 23){
	$cData = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].$this->GetFolder().'/coords.txt'));
	foreach($arResult['ITEMS'] as $k => $arItem){
		if(strlen($arResult['ITEMS'][$k]['PROPERTIES']['MAP']['VALUE']) == 0 && strlen($arItem['PROPERTIES']['ADDRESS']['VALUE']) > 0){
			$params = array(
				'geocode' => $arItem['PROPERTIES']['ADDRESS']['VALUE'], // адрес
				'format'  => 'json',                          // формат ответа
				'results' => 1,                               // количество выводимых результатов
				'key'     => '...',                           // ваш api key
			);
			$response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
 
			if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0)
			{
				$ccc = explode(' ', $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
			}
			CIBlockElement::SetPropertyValuesEx($arItem['ID'], false, array('MAP' => $ccc[1].', '.$ccc[0]));
			$up++;
		}
	}
	if($up > 0){
		?>
		
		<?
	}
}?>
<?
$arShops = $arResult["ITEMS"];

		global $APPLICATION;
		$mapLAT = $mapLON = 0;
		$arPlacemarks = array();

			foreach($arShops as $i => $arShop){
				if(isset($arShop['IBLOCK_ID'])){
					$arShop['TITLE'] = (in_array('NAME', $arParams['FIELD_CODE']) ? strip_tags($arShop['~NAME']) : '');

					$imageID = ((in_array('PREVIEW_PICTURE', $arParams['FIELD_CODE']) && $arShop["PREVIEW_PICTURE"]['ID']) ? $arShop["PREVIEW_PICTURE"]['ID'] : ((in_array('DETAIL_PICTURE', $arParams['FIELD_CODE']) && $arShop["DETAIL_PICTURE"]['ID']) ? $arShop["DETAIL_PICTURE"]['ID'] : false));
					$arShop['IMAGE'] = ($imageID ? CFile::ResizeImageGet($imageID, array('width' => 100, 'height' => 69), BX_RESIZE_IMAGE_EXACT) : '');
					$arShop['ADDRESS'] = $arShop['DISPLAY_PROPERTIES']['ADDRESS']['VALUE'];
					$arShop['ADDRESS'] = $arShop['TITLE'].((strlen($arShop['TITLE']) && strlen($arShop['ADDRESS'])) ? ', ' : '').$arShop['ADDRESS'];
					$arShop['PHONE'] =  $arShop['DISPLAY_PROPERTIES']['PHONE']['VALUE'];
					$arShop['EMAIL'] = $arShop['DISPLAY_PROPERTIES']['EMAIL']['VALUE'];

					if(strToLower($arShop['DISPLAY_PROPERTIES']['SCHEDULE']['VALUE']['TYPE']) == 'html'){
						$arShop['SCHEDULE'] = htmlspecialchars_decode($arShop['DISPLAY_PROPERTIES']['SCHEDULE']['~VALUE']['TEXT']);
					}
					else{
						$arShop['SCHEDULE'] = nl2br($arShop['DISPLAY_PROPERTIES']['SCHEDULE']['~VALUE']['TEXT']);
					}
					$arShop['URL'] = $arShop['DETAIL_PAGE_URL'];
					$arShop['METRO_PLACEMARK_HTML'] = '';
					if($arShop['METRO'] = $arShop['DISPLAY_PROPERTIES']['METRO']['VALUE']){
						if(!is_array($arShop['METRO'])){
							$arShop['METRO'] = array($arShop['METRO']);
						}
						foreach($arShop['METRO'] as $metro){
							$arShop['METRO_PLACEMARK_HTML'] .= '<div class="metro"><i></i>'.$metro.'</div>';
						}
					}
					$arShop['DESCRIPTION'] = $arShop['DETAIL_TEXT'];
					$arShop['GPS_S'] = false;
					$arShop['GPS_N'] = false;
					if($arStoreMap = explode(',', $arShop['DISPLAY_PROPERTIES']['MAP']['VALUE'])){
						$arShop['GPS_S'] = $arStoreMap[0];
						$arShop['GPS_N'] = $arStoreMap[1];
					}
					if($arShop['GPS_S'] && $arShop['GPS_N']){
						$mapLAT += $arShop['GPS_S'];
						$mapLON += $arShop['GPS_N'];
						$str_phones = '';
						if($arShop['PHONE'])
						{
							foreach($arShop['PHONE'] as $phone)
							{
								$str_phones .= '<div class="phone"><a rel="nofollow" href="tel:'.str_replace(array(' ', ',', '-', '(', ')'), '', $phone).'">'.$phone.'</a></div>';
							}
						}
						$arPlacemarks[] = array(
							"ID" => $arShop["ID"],
							"LAT" => $arShop['GPS_S'],
							"LON" => $arShop['GPS_N'],
							"TEXT" => $arShop["TITLE"],
							"HTML" => '<div class="title">'.(strlen($arShop["URL"]) ? '<a href="'.$arShop["URL"].'">' : '').$arShop["ADDRESS"].(strlen($arShop["URL"]) ? '</a>' : '').'</div><div class="info-content">'.($arShop['METRO'] ? $arShop['METRO_PLACEMARK_HTML'] : '').(strlen($arShop['SCHEDULE']) ? '<div class="schedule">'.$arShop['SCHEDULE'].'</div>' : '').$str_phones.(strlen($arShop['EMAIL']) ? '<div class="email"><a rel="nofollow" href="mailto:'.$arShop['EMAIL'].'">'.$arShop['EMAIL'].'</a></div>' : '').'</div>'.(strlen($arShop['URL']) ? '<a rel="nofollow" class="button" href="'.$arShop["URL"].'"><span>'.GetMessage('DETAIL').'</span></a>' : '')
						);
					}
				}
				else{
					$str_phones = '';
					if($arShop['PHONE'])
					{
						$arShop['PHONE'] = explode(",", $arShop['PHONE']);
						foreach($arShop['PHONE'] as $phone)
						{
							$str_phones .= '<div class="phone"><a rel="nofollow" href="tel:'.str_replace(array(' ', ',', '-', '(', ')'), '', $phone).'">'.$phone.'</a></div>';
						}
					}
					if($arShop['GPS_S'] && $arShop['GPS_N']){
						$mapLAT += $arShop['GPS_N'];
						$mapLON += $arShop['GPS_S'];
						$arPlacemarks[] = array(
							"ID" => $arShop["ID"],
							"LON" => $arShop['GPS_S'],
							"LAT" => $arShop['GPS_N'],
							"TEXT" => strip_tags($arShop["TITLE"]),
							"HTML" => '<div class="title">'.(strlen($arShop["URL"]) ? '<a href="'.$arShop["URL"].'">' : '').$arShop["ADDRESS"].(strlen($arShop["URL"]) ? '</a>' : '').'</div><div class="info-content">'.($arShop['METRO'] ? $arShop['METRO_PLACEMARK_HTML'] : '').(strlen($arShop['SCHEDULE']) ? '<div class="schedule">'.$arShop['SCHEDULE'].'</div>' : '').$str_phones.(strlen($arShop['EMAIL']) ? '<div class="email"><a rel="nofollow" href="mailto:'.$arShop['EMAIL'].'">'.$arShop['EMAIL'].'</a></div>' : '').'</div>'.(strlen($arShop['URL']) ? '<a rel="nofollow" class="button" href="'.$arShop["URL"].'"><span>'.GetMessage('DETAIL').'</span></a>' : '')
						);
					}
				}
				$arShops[$i] = $arShop;
			}
			?>
			<?if($arShops):?>
			
				<?if(abs($mapLAT) > 0 && abs($mapLON) > 0 && $showMap=="Y"):?>
					<?
					$mapLAT = floatval($mapLAT / count($arShops));
					$mapLON = floatval($mapLON / count($arShops));
					?>
					<div class="contacts_map">
						<?if($arParams["MAP_TYPE"] != "0"):?>
							<?$APPLICATION->IncludeComponent(
								"bitrix:map.google.view",
								"map",
								array(
									"INIT_MAP_TYPE" => "ROADMAP",
									"MAP_DATA" => serialize(array("google_lat" => $mapLAT, "google_lon" => $mapLON, "google_scale" => 15, "PLACEMARKS" => $arPlacemarks)),
									"MAP_WIDTH" => "100%",
									"MAP_HEIGHT" => "400",
									"CONTROLS" => array(
									),
									"OPTIONS" => array(
										0 => "ENABLE_DBLCLICK_ZOOM",
										1 => "ENABLE_DRAGGING",
									),
									"MAP_ID" => "",
									"ZOOM_BLOCK" => array(
										"POSITION" => "right center",
									),
									"API_KEY" => $arParams["GOOGLE_API_KEY"],
									"COMPOSITE_FRAME_MODE" => "A",
									"COMPOSITE_FRAME_TYPE" => "AUTO"
								),
								false, array("HIDE_ICONS" =>"Y")
							);?>
						<?else:?>
							<?$APPLICATION->IncludeComponent(
								"bitrix:map.yandex.view",
								"",
								array(
									"INIT_MAP_TYPE" => "ROADMAP",
									"MAP_DATA" => serialize(array("yandex_lat" => $mapLAT, "yandex_lon" => $mapLON, "yandex_scale" => 4, "PLACEMARKS" => $arPlacemarks)),
									"MAP_WIDTH" => "100%",
									"MAP_HEIGHT" => "400",
									"CONTROLS" => array(
										0 => "ZOOM",
										1 => "SMALLZOOM",
										3 => "TYPECONTROL",
										4 => "SCALELINE",
									),
									"OPTIONS" => array(
										0 => "ENABLE_DBLCLICK_ZOOM",
										1 => "ENABLE_DRAGGING",
									),
									"MAP_ID" => "",
									"ZOOM_BLOCK" => array(
										"POSITION" => "right center",
									),
									"COMPONENT_TEMPLATE" => "map",
									"API_KEY" => $arParams["GOOGLE_API_KEY"],
									"COMPOSITE_FRAME_MODE" => "A",
									"COMPOSITE_FRAME_TYPE" => "AUTO"
								),
								false, array("HIDE_ICONS" =>"Y")
							);?>
						<?endif;?>
					</div>
				<?endif;?>
				<div class="wrapper_inner">
					<div class="shops list">
						<div class="items">							
							<?foreach($arShops as $arShop):
								if($arShop["IBLOCK_ID"] == 23){
									unset($arShop['URL']);
								}
$db_props = CIBlockElement::GetProperty(23, $arShop['ID'], array("sort" => "asc"), Array("CODE"=>"CITY"));
if($ar_props = $db_props->Fetch()){
    $CITY = $ar_props["VALUE"];
}
							?>
							<?echo $arShop["PROPERTIES"]['CITY']['VALUE']?>
								<?if($CITY == $_COOKIE['regional_city']):?>
								<!-- noindex -->
								<?endif;?>
								<div class="item <?=(strlen($arShop["IMAGE"]["src"]) ? '' : 'wi')?>" data-ID="<?=$arShop['ID']?>">
									<div class="image">
										<?if(strlen($arShop["IMAGE"]["src"])):?>
											<?if(strlen($arShop['URL'])):?>
												<a href="<?=$arShop['URL']?>"><img src="<?=$arShop["IMAGE"]["src"]?>" alt="<?=$arShop["ADDRESS"]?>" title="<?=$arShop["ADDRESS"]?>" /></a>
											<?else:?>
												<img src="<?=$arShop["IMAGE"]["src"]?>" alt="<?=$arShop["ADDRESS"]?>" title="<?=$arShop["ADDRESS"]?>" />
											<?endif;?>
										<?endif;?>
									</div>
									<div class="rubber">
										<div class="title_metro">
											<?if(strlen($arShop["ADDRESS"])):?>
												<?if(strlen($arShop['URL'])):?>
													<a href="<?=$arShop['URL']?>"><div class="title"><?=$arShop["ADDRESS"]?></div></a>
												<?else:?>
													<div class="title"><?=$arShop["ADDRESS"]?></div>
												<?endif;?>
											<?endif;?>
											<?if($arShop["METRO"]):?>
												<?foreach($arShop['METRO'] as $metro):?>
													<div class="metro"><i></i><?=$metro?></div>
												<?endforeach;?>
											<?endif;?>
										</div>
										<div class="schedule_phone_email">
											<div class="schedule"><?=$arShop["SCHEDULE"]?></div>
											<div class="phone_email">
												<?if($arShop["PHONE"]):?>
													<?foreach($arShop["PHONE"] as $phone):?>
														<div class="phone"><a rel="nofollow" href="tel:<?=str_replace(array(' ', ',', '-', '(', ')'), '', $phone);?>"><?=$phone;?></a></div>
													<?endforeach;?>
												<?endif;?>
												<?if(strlen($arShop["EMAIL"])):?>
													<div class="email"><a rel="nofollow" href="mailto:<?=$arShop["EMAIL"]?>"><?=$arShop["EMAIL"]?></a></div>
												<?endif;?>
											</div>
										</div>
									</div>
								</div>
								<?if($CITY == $cN):?>
								<!-- /noindex -->
								<?endif;?>
							<?endforeach;?>
						</div>
					</div>
				</div>
				<div class="clearboth"></div>
			<?endif;?>