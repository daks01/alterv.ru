<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?if($arResult["SECTIONS"]):?>
<div class="catalog_section_list row items sections_wrapper">	
	<?foreach( $arResult["SECTIONS"] as $arItems ):
		$this->AddEditAction($arItems['ID'], $arItems['EDIT_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItems['ID'], $arItems['DELETE_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<h3 class="title_block blue_underlined">
			<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="dark_link"><span><?=$arItems["NAME"]?></span></a> 
		</h3>
		<?if($arItems["SECTIONS"]):?>
			<div class="list items">
				<div class="row margin0">
				<?foreach( $arItems["SECTIONS"] as $arItem ):?>
					<div class="col-m-20 col-md-3 col-sm-4 col-xs-6">
						<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<div class="img shine">
								<?if($arItem["PICTURE"]["SRC"]):?>
									<?$img = CFile::ResizeImageGet($arItem["PICTURE"]["ID"], array( "width" => 120, "height" => 120 ), BX_RESIZE_IMAGE_EXACT, true );?>
									<a href="<?=$arItem["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=$img["src"]?>" alt="<?=($$arItem["PICTURE"]["ALT"] ? $arItem["PICTURE"]["ALT"] : $arItem["NAME"])?>" title="<?=($arItem["PICTURE"]["TITLE"] ? $arItem["PICTURE"]["TITLE"] : $arItem["NAME"])?>" /></a>
								<?elseif($arItem["~PICTURE"]):?>
									<?$img = CFile::ResizeImageGet($arItem["~PICTURE"], array( "width" => 120, "height" => 120 ), BX_RESIZE_IMAGE_EXACT, true );?>
									<a href="<?=$arItem["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=$img["src"]?>" alt="<?=($$arItem["PICTURE"]["ALT"] ? $arItem["PICTURE"]["ALT"] : $arItem["NAME"])?>" title="<?=($arItem["PICTURE"]["TITLE"] ? $$arItem["PICTURE"]["TITLE"] : $arItem["NAME"])?>" /></a>
								<?else:?>
									<a href="<?=$arItem["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" height="90" /></a>
								<?endif;?>
							</div>
							<div class="name">
								<a href="<?=$arItem['SECTION_PAGE_URL'];?>" class="dark_link"><?=$arItem['NAME'];?></a>
							</div>
						</div>
					</div>
				<?endforeach;?>
				</div>
			</div>
		<?endif;?>
	<?endforeach;?>
</div>
<?endif;?>