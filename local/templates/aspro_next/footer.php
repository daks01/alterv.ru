						<?CNext::checkRestartBuffer();?>
						<?IncludeTemplateLangFile(__FILE__);?>
							<?if(!$isIndex):?>
								<?if($isBlog):?>
									</div> <?// class=col-md-9 col-sm-9 col-xs-8 content-md?>
									<div class="col-md-3 col-sm-3 hidden-xs hidden-sm right-menu-md">
										<div class="sidearea">
											<?$APPLICATION->ShowViewContent('under_sidebar_content');?>
											<?CNext::get_banners_position('SIDE');?>
											<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "sect", "AREA_FILE_SUFFIX" => "sidebar", "AREA_FILE_RECURSIVE" => "Y"), false);?>
										</div>
									</div>
								<?endif;?>
								<?if($isHideLeftBlock):?>
									</div> <?// .maxwidth-theme?>
								<?endif;?>
								</div> <?// .container?>
							<?else:?>
								<?CNext::ShowPageType('indexblocks');?>
							<?endif;?>
							<?CNext::get_banners_position('CONTENT_BOTTOM');?>
						</div> <?// .middle?>
					<?//if(!$isHideLeftBlock && !$isBlog):?>
					<?if(($isIndex && $isShowIndexLeftBlock) || (!$isIndex && !$isHideLeftBlock) && !$isBlog):?>
						</div> <?// .right_block?>				
						<?if($APPLICATION->GetProperty("HIDE_LEFT_BLOCK") != "Y" && !defined("ERROR_404")):?>
							<div class="left_block">
								<?CNext::ShowPageType('left_block');?>
							</div>
						<?endif;?>
					<?endif;?>
				<?if($isIndex):?>
					</div>
				<?elseif(!$isWidePage):?>
					</div> <?// .wrapper_inner?>				
				<?endif;?>
			</div> <?// #content?>
			<?CNext::get_banners_position('FOOTER');?>
		</div><?// .wrapper?>
		<footer id="footer">
			<?/*if($APPLICATION->GetProperty("viewed_show") == "Y" || $is404):?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include", 
					"basket", 
					array(
						"COMPONENT_TEMPLATE" => "basket",
						"PATH" => SITE_DIR."include/footer/comp_viewed.php",
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "",
						"AREA_FILE_RECURSIVE" => "Y",
						"EDIT_TEMPLATE" => "standard.php",
						"PRICE_CODE" => array(
							0 => "BASE",
						),
						"STORES" => array(
							0 => "",
							1 => "",
						),
						"BIG_DATA_RCM_TYPE" => "bestsell"
					),
					false
				);?>					
			<?endif;*/?>
			<?CNext::ShowPageType('footer');?>
		</footer>
		<div class="bx_areas">
			<?CNext::ShowPageType('bottom_counter');?>
		</div>
		<?CNext::ShowPageType('search_title_component');?>
		<?CNext::setFooterTitle();
		CNext::showFooterBasket();?>
		<div class="modal fade bs-example-modal-sm" id="by1click" tabindex="-1" role="dialog" aria-labelledby="by1click">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Купить в 1 клик</h4>
      				</div>
	  				<div class="modal-body">
		  				<div class="one_click_new_form">
			  				<?if($_COOKIE['regional_city']=='Москва'):?>				
								<?$APPLICATION->IncludeComponent(
									"bitrix:form.result.new",
									"inline",
									Array(
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CHAIN_ITEM_LINK" => "",
										"CHAIN_ITEM_TEXT" => "",
										"COMPONENT_TEMPLATE" => "inline",
										"EDIT_URL" => "",
										"IGNORE_CUSTOM_TEMPLATE" => "N",
										"LIST_URL" => "",
										"SEF_MODE" => "N",
										"SUCCESS_URL" => "?send=ok",
										"USE_EXTENDED_ERRORS" => "N",
										"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
										"WEB_FORM_ID" => "9"
									)
								);?>
								<script>
									$(function(){									  
									  $('.one_click_new_form form input[name="form_text_42"]').mask("+7(999) 999-99-99");
									});
									
									$(document).ready(function(){
										
										$('.one_click').click(function(){
											$('.one_click_new_form form input[name="form_text_41"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_42"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_43"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_46"]').attr('required','required');		
											$('.one_click_new_form form input[name="form_text_48"]').attr('value','http://'+$(this).attr('data-href'));
										});																						
										
										$('.one_click_new_form form').submit(function(){
											var error=0;											
											var tovar = $('.one_click_new_form form input[name="form_text_48"]').val();
											
											if(tovar==''){
												error=1;
											}
											
											if(error==0){
												return true;											
											}
											else{
												return false;
											}
										});
									})																
								</script>
							<?endif?>
							
							<?if($_COOKIE['regional_city']=='Екатеринбург'):?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:form.result.new",
									"inline",
									Array(
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CHAIN_ITEM_LINK" => "",
										"CHAIN_ITEM_TEXT" => "",
										"COMPONENT_TEMPLATE" => "inline",
										"EDIT_URL" => "",
										"IGNORE_CUSTOM_TEMPLATE" => "N",
										"LIST_URL" => "",
										"SEF_MODE" => "N",
										"SUCCESS_URL" => "?send=ok",
										"USE_EXTENDED_ERRORS" => "N",
										"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
										"WEB_FORM_ID" => "12"
									)
								);?>
								<script>
									$(function(){									  
									  $('.one_click_new_form form input[name="form_text_53"]').mask("+7(999) 999-99-99");
									});
									
									$(document).ready(function(){
										$('.one_click').click(function(){
											$('.one_click_new_form form input[name="form_text_52"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_53"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_54"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_57"]').attr('required','required');		
											$('.one_click_new_form form input[name="form_text_51"]').attr('value','http://'+$(this).attr('data-href'));															
										});
										
										$('.one_click_new_form form').submit(function(){
											error=0;											
											tovar = $('.one_click_new_form form input[name="form_text_51"]').val();
											
											if(tovar==''){
												error=1;
											}
											
											if(error==0){
												return true;											
											}
											else{
												return false;
											}
										});
									})																		
								</script>
							<?endif?>
							<?if($_COOKIE['regional_city']=='Ростов-на-Дону'):?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:form.result.new",
									"inline",
									Array(
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CHAIN_ITEM_LINK" => "",
										"CHAIN_ITEM_TEXT" => "",
										"COMPONENT_TEMPLATE" => "inline",
										"EDIT_URL" => "",
										"IGNORE_CUSTOM_TEMPLATE" => "N",
										"LIST_URL" => "",
										"SEF_MODE" => "N",
										"SUCCESS_URL" => "?send=ok",
										"USE_EXTENDED_ERRORS" => "N",
										"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
										"WEB_FORM_ID" => "13"
									)
								);?>
								<script>
									$(function(){									  
									  $('.one_click_new_form form input[name="form_text_61"]').mask("+7(999) 999-99-99");
									});
									
									$(document).ready(function(){
										$('.one_click').click(function(){
											$('.one_click_new_form form input[name="form_text_60"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_61"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_62"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_65"]').attr('required','required');		
											$('.one_click_new_form form input[name="form_text_59"]').attr('value','http://'+$(this).attr('data-href'));			
										});
										
										$('.one_click_new_form form').submit(function(){
											error=0;											
											tovar = $('.one_click_new_form form input[name="form_text_59"]').val();
											
											if(tovar==''){
												error=1;
											}
											
											if(error==0){
												return true;											
											}
											else{
												return false;
											}
										});												
									})																		
								</script>
							<?endif?>
							<?if($_COOKIE['regional_city']=='Новосибирск'):?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:form.result.new",
									"inline",
									Array(
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CHAIN_ITEM_LINK" => "",
										"CHAIN_ITEM_TEXT" => "",
										"COMPONENT_TEMPLATE" => "inline",
										"EDIT_URL" => "",
										"IGNORE_CUSTOM_TEMPLATE" => "N",
										"LIST_URL" => "",
										"SEF_MODE" => "N",
										"SUCCESS_URL" => "?send=ok",
										"USE_EXTENDED_ERRORS" => "N",
										"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
										"WEB_FORM_ID" => "17"
									)
								);?>
								<script>
									$(function(){									  
									  $('.one_click_new_form form input[name="form_text_81"]').mask("+7(999) 999-99-99");
									});
									
									$(document).ready(function(){
										$('.one_click').click(function(){
											$('.one_click_new_form form input[name="form_text_80"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_81"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_82"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_85"]').attr('required','required');		
											$('.one_click_new_form form input[name="form_text_79"]').attr('value','http://'+$(this).attr('data-href'));		
										});
										
										$('.one_click_new_form form').submit(function(){
											error=0;											
											tovar = $('.one_click_new_form form input[name="form_text_79"]').val();
											
											if(tovar==''){
												error=1;
											}
											
											if(error==0){
												return true;											
											}
											else{
												return false;
											}
										});													
									})																		
								</script>
							<?endif?>
							<?if($_COOKIE['regional_city']=='Санкт-Петербург'):?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:form.result.new",
									"inline",
									Array(
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CHAIN_ITEM_LINK" => "",
										"CHAIN_ITEM_TEXT" => "",
										"COMPONENT_TEMPLATE" => "inline",
										"EDIT_URL" => "",
										"IGNORE_CUSTOM_TEMPLATE" => "N",
										"LIST_URL" => "",
										"SEF_MODE" => "N",
										"SUCCESS_URL" => "?send=ok",
										"USE_EXTENDED_ERRORS" => "N",
										"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
										"WEB_FORM_ID" => "18"
									)
								);?>
								<script>
									$(function(){									  
									  $('.one_click_new_form form input[name="form_text_91"]').mask("+7(999) 999-99-99");
									});
									
									$(document).ready(function(){
										$('.one_click').click(function(){
											$('.one_click_new_form form input[name="form_text_89"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_91"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_92"]').attr('required','required');
											$('.one_click_new_form form input[name="form_text_95"]').attr('required','required');		
											$('.one_click_new_form form input[name="form_text_97"]').attr('value','http://'+$(this).attr('data-href'));		
										});
										
										$('.one_click_new_form form').submit(function(){
											error=0;											
											tovar = $('.one_click_new_form form input[name="form_text_97"]').val();
											
											if(tovar==''){
												error=1;
											}
											
											if(error==0){
												return true;											
											}
											else{
												return false;
											}
										});													
									})																		
								</script>
							<?endif?>
						</div>
      				</div>      
    			</div>
  			</div>
		</div>
		<?
			if(isset($_GET['vz'])){																				
				
			}
		?>
		<?if($_GET['send']=='ok' && $_GET['WEB_FORM_ID']!='' && $_GET['RESULT_ID']!=''):?>
			<div class="success_by1click">Сообщение отправлено</div>
			<style>
				.success_by1click{
					position: fixed;
					top: 30%;
					left: 45%;
					background: #1976d2;
					z-index: 10010;
					padding: 20px;
					border-radius: 5px;
					color: #fff;
					box-shadow: 0 0 15px 1px #000;
				}
			</style>
			<script>setTimeout(function() { $('.success_by1click').hide('slow') }, 3000);</script>		
			
			<?
				if($_COOKIE['regional_city']=='Новосибирск'){
					$RESULT_ID = $_GET['RESULT_ID'];
					$arAnswer = CFormResult::GetDataByID($RESULT_ID, array(), $arResult, $arAnswer2);					
					
					if($arAnswer)
					{
						$to = 'nsk@alterv.ru';
	
					    // тема письма
					    $subject = 'Новый заказ(Новосибирск)';
					        
					    // текст письма
					    $message = '					    
						Имя клиента: '.$arAnswer['new_field_67977'][0]['USER_TEXT'].'<br>
						Телефон: '.$arAnswer['new_field_93104'][0]['USER_TEXT'].'<br>
						E-mail: '.$arAnswer['new_field_48604'][0]['USER_TEXT'].'<br>
						Название компании: '.$arAnswer['new_field_455'][0]['USER_TEXT'].'<br>
						Номер заказа: '.$arAnswer['new_field_74772'][0]['USER_TEXT'].'<br>
						Город: '.$arAnswer['new_field_74803'][0]['USER_TEXT'].'<br>
						Комментарий: '.$arAnswer['new_field_49709'][0]['USER_TEXT'].'<br>
						Товар: '.$arAnswer['tovar'][0]['USER_TEXT'].'<br>
						Заказ создан: '.date('Y-m-d');
					
				        // Для отправки HTML-письма должен быть установлен заголовок Content-type
				        $headers = 'MIME-Version: 1.0' . "\r\n";
				        $headers .= "Content-type: text/html; charset=utf-8 \r\n";
					    $headers .= "From: alterv.ru <info@alterv.ru>\r\n";
					
					    // Отправляем
					    mail($to, $subject, $message, $headers);
					}					
				}
			?>
			
			<?
				if($_COOKIE['regional_city']=='Санкт-Петербург'){
					$RESULT_ID = $_GET['RESULT_ID'];
					$arAnswer = CFormResult::GetDataByID($RESULT_ID, array(), $arResult, $arAnswer2);					
					
					if($arAnswer)
					{
						$to = 'spb@alterv.ru';
	
					    // тема письма
					    $subject = 'Новый заказ(Санкт-Петербург)';
					        
					    // текст письма
					    $message = '					    
						Имя клиента: '.$arAnswer['new_field_99374'][0]['USER_TEXT'].'<br>
						Телефон: '.$arAnswer['new_field_55213'][0]['USER_TEXT'].'<br>
						E-mail: '.$arAnswer['new_field_71110'][0]['USER_TEXT'].'<br>
						Название компании: '.$arAnswer['new_field_90596'][0]['USER_TEXT'].'<br>
						Номер заказа: '.$arAnswer['new_field_26251'][0]['USER_TEXT'].'<br>
						Город: '.$arAnswer['new_field_44677'][0]['USER_TEXT'].'<br>
						Комментарий: '.$arAnswer['new_field_27206'][0]['USER_TEXT'].'<br>
						Товар: '.$arAnswer['tovar'][0]['USER_TEXT'].'<br>
						Заказ создан: '.date('Y-m-d');
					
				        // Для отправки HTML-письма должен быть установлен заголовок Content-type
				        $headers = 'MIME-Version: 1.0' . "\r\n";
				        $headers .= "Content-type: text/html; charset=utf-8 \r\n";
					    $headers .= "From: alterv.ru <info@alterv.ru>\r\n";
					
					    // Отправляем
					    mail($to, $subject, $message, $headers);
					}					
				}
			?>
				
		<?endif?>
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript" >
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
					try {
						w.yaCounter10256857 = new Ya.Metrika({
							id:10256857,
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true,
							webvisor:true
						});
					} catch(e) { }
				});

				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = "https://mc.yandex.ru/metrika/watch.js";

				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/10256857" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->
		<script type="text/javascript" src="/local/templates/aspro_next/js/owl.carousel.min.js"></script>
		<script type="text/javascript" src="/local/templates/aspro_next/js/slider.js"></script>
		<script type="text/javascript" src="/local/templates/aspro_next/js/jquery.maskedinput.min.js"></script>				
		<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	</body>
</html>
<?

if($_GET['region'] > 0){
	LocalRedirect(substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')));
}
?>